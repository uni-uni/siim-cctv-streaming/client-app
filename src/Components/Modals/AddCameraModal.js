import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import BaseSwitch from '../Switches/BaseSwitch'

export default function AddCameraModal(props) {
    const [isOpen, setIsOpen] = useState(false)

    const handleUrlChange = (event) => {
        props.setUrl(event.target.value)
    }

    const sumbit = () => {
        setIsOpen(false)
        props.handleCameraAdd()
    }

    const handleSwitch = () => {
        props.setEnabled(!props.enabled)
    }

    return (
        <>
            <div>
                <button class="bg-teal-500 hover:bg-teal-700 ml-10 text-white font-bold py-2 px-10 rounded" onClick={() => setIsOpen(true)}>
                    Add Camera
                </button>
            </div>

            <Transition appear show={isOpen} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={() => setIsOpen(false)}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-black bg-opacity-25" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-y-auto">
                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >
                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                    <span className="absolute top-0 bottom-0 right-0 px-4 py-3" onClick={() => setIsOpen(false)}>
                                        <svg className="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" /></svg>
                                    </span>
                                    <Dialog.Title
                                        as="h3"
                                        className="text-lg font-medium leading-6 text-gray-900"
                                    >
                                        Add new camera
                                    </Dialog.Title>
                                    <div className="my-2">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" for="username">
                                            URL Address
                                        </label>
                                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="e.g. 127.0.0.1" onChange={handleUrlChange} />
                                    </div>
                                    <div className="my-2 -ml-10">
                                        <BaseSwitch desc="Turn on camera?" name="" enabled={props.enabled} handleChange={handleSwitch} modal={true} />
                                    </div>
                                    <div className="my-2">
                                        <button
                                            type="button"
                                            className="inline-flex justify-center rounded-md border border-transparent bg-teal-500 px-4 py-2 text-sm font-medium text-white hover:bg-teal-700 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                                            onClick={sumbit}
                                        >
                                            Done
                                        </button>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        </>
    )
}