import { Switch } from '@headlessui/react'

export default function BaseSwitch(props) {

    return (
        <div className={"flex items-center ml-10 my-5"}>
            <label className={props.modal ? "mr-4 w-40" : "mr-4 w-96"}>
                {props.desc}
            </label>
            <Switch.Group>
                <Switch
                    checked={props.enabled}
                    onChange={props.handleChange}
                    className={`${props.enabled ? 'bg-blue-600' : "bg-gray-200"} relative inline-flex h-6 w-11 items-center rounded-full transition-colors focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2`}>
                    <span className={`${props.enabled ? "translate-x-6" : "translate-x-1"} inline-block h-4 w-4 transform rounded-full bg-white transition-transform`} />
                </Switch>
            </Switch.Group>
        </div>
    )
}